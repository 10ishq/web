# %%
import os
import tensorflow as tf
from tensorflow.keras.preprocessing import image

import PIL
import os
from PIL import Image, ImageOps
import random


fsource = r'D:/megadetector/output/people/'
fdest = r'D:/megadetector/output/destination_folder/'

# %%
new_model = tf.keras.models.load_model('saved_model/my_model')
class_indices = {0: 'animal', 1: 'empty'}


# %%

import shutil


os.listdir(fsource)
if not os.path.exists(fdest):
    os.makedirs(fdest)

# %%
for file in os.listdir(fsource):
    f_img = fsource+"/"+file
    img = Image.open(f_img)
    img = img.resize((255, 255))
    #print("New size:", img.size)
    img = image.img_to_array(img)/255
    img = tf.expand_dims(img,axis=0)
    print("Image Shape",img.shape)
    prediction = int(tf.round(new_model.predict(x=img)).numpy()[0][0])
    print("The predicted value is: ",prediction,"and the predicted label is:",class_indices[prediction])
    if(class_indices[prediction]=='animal'):
        animalPath = os.path.join(fdest+"animals/")
        if not os.path.exists(animalPath):
            os.makedirs(animalPath)
        shutil.copy(fsource+file, animalPath)   
    if(class_indices[prediction]=='empty'):
        emptyPath = os.path.join(fdest+"empty/")
        if not os.path.exists(emptyPath):
            os.makedirs(emptyPath)
        shutil.copy(fsource+file, emptyPath)       
            
    # img.show(f_img)
    #img.save('empty_resize/crop_' + str(file), 'PNG')

# %%



