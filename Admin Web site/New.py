import os
from django.shortcuts import render
from flask import Flask, render_template, request, redirect, send_file, url_for
from constant import s3Client, bucket_name, Wildeyeid
from werkzeug.utils import secure_filename
import requests
import json
from geopy.geocoders import Nominatim

app = Flask(__name__)

uploadfile = "uploads/"

@app.route("/")
def front():
    return render_template("index.html")
@app.route("/deployp")
def dep():
    return render_template("DeployPage.html")
@app.route("/uploadimg")
def upl():
    return redirect("/upload")

@app.route("/data", methods=["POST"])
def address():
    out = request.get_json()
    print(out)
    res = json.loads(out) 
    cord = [res["Latitude"],res["Longitude"]]    
    geoLoc = Nominatim(user_agent="GetLoc")
    locname = geoLoc.reverse(cord)
    add = locname.address
    myadd = add.split(",")
    print(myadd)
    def int_filter( someList ):
        for v in someList:
            try:
                int(v)
                continue # Skip these
            except ValueError:
                yield v # Keep these    
    myadd = (list(int_filter(myadd)))
    print(myadd)
    city= myadd[-3]
    state= myadd[-2]
    country= myadd[-1]
    url = "https://wsw1q1bawl.execute-api.ap-south-1.amazonaws.com/deployed/wild"
    postdata = {
    "WildeyeID":str( res["WildID"]),
    "Latitude":str( res["Latitude"]),
    "Longitude":str( res["Longitude"]),
    "City":str(city),
    "State":str(state),
    "Country":str(country)
    }
    resq = requests.post(url, json=postdata) 
    print("Status Code", resq.status_code)
    print("JSON Response ", resq.json())
    resp = s3Client.put_object(
    Bucket = "trail-images-first",
    Key = str( res["WildID"])+"/"
    )
    print(resp)
    return render_template("DeployPage.html") 

@app.route("/upload", methods=['GET','POST'])
     
def upload():
    dir = 'uploads/'
    for f in os.listdir(dir):
        os.remove(os.path.join(dir, f)) 
    ms =""
    if request.method == "POST":
        Id = request.form["id"]
        if str(Id) in Wildeyeid:
            files = request.files.getlist('file')
            if files[0].filename == '' :
                ms = "Select a file"
            else:
                for file in files:
                    fil = secure_filename(file.filename)
                    path = os.path.join(uploadfile,fil)
                    file.save(path)
                    # generate the presigned url
                    resp = s3Client.generate_presigned_post(
                    Bucket = bucket_name,
                    Key = str(Id)+"/" + fil,
                    ExpiresIn = 100
                    )
                    fi = {'file': open(path,'rb')}
                    r = requests.post(resp['url'], data= resp['fields'], files=fi)
                    print(r.status_code)                  
                ms = "Successfully uploaded"              
        elif str(Id) not in Wildeyeid:
            ms = "Id is not present"   
    return render_template('Uploadpage.html',ids = Wildeyeid, len = len(Wildeyeid) ,msg = ms)   



if  __name__ == "__main__":
    app.run(debug=True)
