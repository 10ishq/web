from constant import data, buckets,s3Client,bucket
from flask import Flask, render_template, jsonify, redirect,request
import requests , json,sys
app =Flask(__name__)

@app.route("/")
def map():
    return render_template("maps.html", data = data) 
@app.route("/data", methods=["POST"])
def kuc():
    coordinates = request.get_json()
    coord = json.loads(coordinates)
    global latj, longj
    latj= str(coord['lat'])
    longj= str(coord['long'])  
    return "suc"
def show(latj,longj):    
    wildid = ""
    public_urls=[]
    for i in data:
        latt =str(i["Latitude"])
        longg =str(i["Longitude"])
        if latt == latj and longg == longj:
            wildid = str(i["WildeyeID"])+"/"
    for buck in buckets:
        folderName = str(buck["Key"])
        if(wildid in folderName):
            presigned_url = s3Client.generate_presigned_url(
                'get_object',
                Params = {
                     'Bucket': bucket,
                     'Key': folderName
                     },
                ExpiresIn = 100
            )
            if wildid+"?" not in presigned_url:
                public_urls.append(presigned_url)            
    print(public_urls)            
    return tuple(public_urls)
    
@app.route("/Images")
def img():
    imgurl = show(latj,longj)
    return render_template("collection.html", contents = imgurl, len = len(imgurl))
if __name__ == '__main__':
    app.run(debug=True)